<!DOCTYPE html>
<html itemscope itemtype="http://schema.org/Other">

<head prefix="og: http://ogp.me/ns# profile: http://ogp.me/ns/profile#">
  <meta charset="UTF-8">
  <title>new project</title>
  <meta name="robots" content="noindex, nofollow">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <link rel="shortcut icon" href="../assets/img/favicons/favicon.ico" type="image/x-icon">
  <link rel="icon" href="../assets/img/favicons/favicon.ico" type="image/x-icon">
  <meta name="msapplication-TileImage" content="../assets/img/favicons/ms-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="180x180" href="../assets/img/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="../assets/img/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="../assets/img/favicons/favicon-16x16.png">
  <link rel="mask-icon" href="../assets/img/favicons/safari-pinned-tab.svg" color="#5bbad5">
   <link rel="stylesheet" href="../assets/css/app.css">        
</head>

<body class="app" data-lang="lt">
    <div class="app__page page" data-page> 
