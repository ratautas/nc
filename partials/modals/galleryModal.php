<div class="app__modal modal" data-modal="gallery">
  <div class="modal__dialog" data-modal-prevent>
    <div class="modal__top">
      <div class="modal__close close" data-modal-close></div>
    </div>
    <div class="modal__content">
      <div class="gallery">
        <div class="gallery__containter swiper-container" data-gallery-swiper>
          <div class="gallery__wrapper swiper-wrapper">
            <div class="gallery__slide swiper-slide">
              <img class="gallery__img" src="../assets/img/product.png" alt="">
            </div>
            <div class="gallery__slide swiper-slide">
              <img class="gallery__img" src="../assets/img/product2.png" alt="">
            </div>
            <div class="gallery__slide swiper-slide">
              <img class="gallery__img" src="../assets/img/product3.png" alt="">
            </div>
            <div class="gallery__slide swiper-slide">
              <img class="gallery__img" src="../assets/img/product4.png" alt="">
            </div>
          </div>
          </div>
          <div class="gallery__nav gallery__nav--prev" data-gallery-prev></div>
          <div class="gallery__nav gallery__nav--next" data-gallery-next></div>
      </div>
    </div>
  </div>
</div>