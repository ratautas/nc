var _db = function(f, w, i) {
  var t;
  return function() {
    var s = this,
      a = arguments;
    var later = function() {
      t = null;
      if (!i) f.apply(s, a);
    };
    var a = i && !t;
    clearTimeout(t);
    t = setTimeout(later, w);
    if (a) f.apply(s, a);
  };
};
