_nc.forms = {};

_nc.forms.init = function() {
  if ($('[data-form]').length) {
    $('[data-form]').each(function() {
      var $form = $(this);
      var name = $form.attr('data-form');
      console.log(name);
      $form.parsley();
      $form.on('submit', function(e) {
        e.preventDefault();
        var formObj = new FormData();
        var endpoint = null;
        var openModal = function() {
          _nc.modal.open(name + '-success');
        };
        $form.find('input').each(function() {
          formObj.append($(this).attr('name'), $(this).val());
        });
        $form.find('textarea').each(function() {
          formObj.append($(this).attr('name'), $(this).val());
        });
        if (name === 'wedding') {
          endpoint = '../endpoints/weddingsSubmit.php';
        }
        $.ajax({
          url: endpoint,
          data: formObj,
          processData: false,
          contentType: 'multipart/form-data',
          type: 'POST',
          success: function(data) {
            openModal();
          }
        });
      });
    });
  }
};
