if (typeof _nc === 'undefined') {
  var _nc = {};
}

_nc.i18n = {};
_nc.i18n.slug = document.querySelector('[data-lang]').getAttribute('data-lang');
_nc.i18n.parsley = function() {
  if (_nc.i18n.slug == 'lt') {
    Parsley.addMessages('lt', {
      defaultMessage: 'Šis įrašas neteisingas.',
      type: {
        email: 'Šis įrašas nėra teisingas el. paštas.',
        url: 'Šis įrašas nėra teisingas url.',
        number: 'Šis įrašas nėra skaičius.',
        integer: 'Šis įrašas nėra sveikasis skaičius.',
        digits: 'Šis įrašas turi būti skaičius.',
        alphanum: 'Šis įrašas turi būti iš skaičių ir raidžių.'
      },
      notblank: 'Šis įrašas negali būti tuščias.',
      required: 'Šis įrašas yra privalomas',
      pattern: 'Šis įrašas neteisingas.',
      min: 'Ši vertė turi būti didesnė arba lygi %s.',
      max: 'Ši vertė turi būti mažesnė arba lygi %s.',
      range: 'Ši vertė turi būti tarp %s ir %s.',
      minlength:
        'Šis įrašas per trumpas. Jis turi turėti %s simbolius arba daugiau.',
      maxlength:
        'Šis įrašas per ilgas. Jis turi turėti %s simbolius arba mažiau.',
      length:
        'Šio įrašo ilgis neteisingas. Jis turėtų būti tarp %s ir %s simbolių.',
      mincheck: 'Jūs turite pasirinkti bent %s pasirinkimus.',
      maxcheck: 'Jūs turite pasirinkti ne daugiau %s pasirinkimų.',
      check: 'Jūs turite pasirinkti tarp %s ir %s pasirinkimų.',
      equalto: 'Ši reikšmė turėtų būti vienoda.'
    });
    Parsley.setLocale('lt');
  } else if (_nc.i18n.slug == 'ru') {
    Parsley.addMessages('ru', {
      defaultMessage: 'Некорректное значение.',
      type: {
        email: 'Введите адрес электронной почты.',
        url: 'Введите URL адрес.',
        number: 'Введите число.',
        integer: 'Введите целое число.',
        digits: 'Введите только цифры.',
        alphanum: 'Введите буквенно-цифровое значение.'
      },
      notblank: 'Это поле должно быть заполнено.',
      required: 'Обязательное поле.',
      pattern: 'Это значение некорректно.',
      min: 'Это значение должно быть не менее чем %s.',
      max: 'Это значение должно быть не более чем %s.',
      range: 'Это значение должно быть от %s до %s.',
      minlength: 'Это значение должно содержать не менее %s символов.',
      maxlength: 'Это значение должно содержать не более %s символов.',
      length: 'Это значение должно содержать от %s до %s символов.',
      mincheck: 'Выберите не менее %s значений.',
      maxcheck: 'Выберите не более %s значений.',
      check: 'Выберите от %s до %s значений.',
      equalto: 'Это значение должно совпадать.'
    });

    Parsley.setLocale('ru');
  } else {
    Parsley.addMessages('en', {
      defaultMessage: 'This value seems to be invalid.',
      type: {
        email: 'This value should be a valid email.',
        url: 'This value should be a valid url.',
        number: 'This value should be a valid number.',
        integer: 'This value should be a valid integer.',
        digits: 'This value should be digits.',
        alphanum: 'This value should be alphanumeric.'
      },
      notblank: 'This value should not be blank.',
      required: 'This value is required.',
      pattern: 'This value seems to be invalid.',
      min: 'This value should be greater than or equal to %s.',
      max: 'This value should be lower than or equal to %s.',
      range: 'This value should be between %s and %s.',
      minlength:
        'This value is too short. It should have %s characters or more.',
      maxlength:
        'This value is too long. It should have %s characters or fewer.',
      length:
        'This value length is invalid. It should be between %s and %s characters long.',
      mincheck: 'You must select at least %s choices.',
      maxcheck: 'You must select %s choices or fewer.',
      check: 'You must select between %s and %s choices.',
      equalto: 'This value should be the same.'
    });
    Parsley.setLocale('en');
  }
}();
