if (typeof _nc === 'undefined') {
  var _nc = {};
}

_nc.scrollFix = function() {
  document.body.style.paddingRight =
    window.innerWidth - document.body.clientWidth + 'px';
  document.body.style.touchAction = 'none';
  document.body.style.overflow = 'hidden';
  document.body.style.pointerEvents = 'none';
  document.documentElement.style.paddingRight =
    window.innerWidth - document.body.clientWidth + 'px';
  document.documentElement.style.touchAction = 'none';
  document.documentElement.style.overflow = 'hidden';
  document.documentElement.style.pointerEvents = 'none';
};

_nc.scrollUnFix = function(time) {
  document.body.removeAttribute('style');
  document.documentElement.removeAttribute('style');
};
