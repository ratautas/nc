function placeholderPolyfill() {
  this.classList[this.value ? 'remove' : 'add']('placeholder-shown');
}

document.querySelectorAll('[placeholder]').forEach(function(el) {
  el.addEventListener('change', placeholderPolyfill);
  el.addEventListener('input', placeholderPolyfill);
  el.addEventListener('keyup', placeholderPolyfill);
});
