if (typeof _nc === 'undefined') {
  var _nc = {};
}

_nc.fade = {};

_nc.fade.in = function($el, cb) {
  var time = Number($el.css('transition-duration').slice(0, -1)) * 1000;
  $el.addClass('is-entering');
  setTimeout(function() {
    $el.addClass('is-active');
  }, 1);
  setTimeout(function() {
    $el.removeClass('is-entering');
    cb ? cb() : false;
  }, time + 1);
};

_nc.fade.out = function($el, cb) {
  var time = Number($el.css('transition-duration').slice(0, -1)) * 1000;
  $el.addClass('is-leaving');
  setTimeout(function() {
    $el.removeClass('is-leaving');
    $el.removeClass('is-active');
    cb ? cb() : false;
  }, 1 + time);
};
