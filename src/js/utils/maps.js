_nc.maps = {};

_nc.maps.init = function() {
  $('[data-map]').each(function() {
    console.log('hi');
    var mapName = $(this).attr('data-map'),
      markerName = mapName + 'Marker',
      markerImg = '../assets/img/pin.png',
      markerLat = Number($(this).attr('data-map-marker-lat')),
      markerLng = Number($(this).attr('data-map-marker-lng')),
      centerLat = Number($(this).attr('data-map-center-lat')),
      centerLng = Number($(this).attr('data-map-center-lng')),
      mapZoom = Number($(this).attr('data-map-zoom')),
      markerPosition = new google.maps.LatLng(markerLat, markerLng);
    if (_nc.device.winW < 769) {
      centerLat = markerLat;
      centerLng = markerLng;
    }
    _nc.maps[mapName] = new google.maps.Map($(this)[0], {
      styles: [
        {
          featureType: 'administrative',
          elementType: 'all',
          stylers: [
            {
              saturation: '-100'
            }
          ]
        },
        {
          featureType: 'administrative.province',
          elementType: 'all',
          stylers: [
            {
              visibility: 'off'
            }
          ]
        },
        {
          featureType: 'landscape',
          elementType: 'all',
          stylers: [
            {
              saturation: -100
            },
            {
              lightness: 65
            },
            {
              visibility: 'on'
            }
          ]
        },
        {
          featureType: 'poi',
          elementType: 'all',
          stylers: [
            {
              saturation: -100
            },
            {
              lightness: '50'
            },
            {
              visibility: 'simplified'
            }
          ]
        },
        {
          featureType: 'road',
          elementType: 'all',
          stylers: [
            {
              saturation: '-100'
            }
          ]
        },
        {
          featureType: 'road.highway',
          elementType: 'all',
          stylers: [
            {
              visibility: 'simplified'
            }
          ]
        },
        {
          featureType: 'road.arterial',
          elementType: 'all',
          stylers: [
            {
              lightness: '30'
            }
          ]
        },
        {
          featureType: 'road.local',
          elementType: 'all',
          stylers: [
            {
              lightness: '40'
            }
          ]
        },
        {
          featureType: 'transit',
          elementType: 'all',
          stylers: [
            {
              saturation: -100
            },
            {
              visibility: 'simplified'
            }
          ]
        },
        {
          featureType: 'water',
          elementType: 'geometry',
          stylers: [
            {
              hue: '#ffff00'
            },
            {
              lightness: -25
            },
            {
              saturation: -97
            }
          ]
        },
        {
          featureType: 'water',
          elementType: 'labels',
          stylers: [
            {
              lightness: -25
            },
            {
              saturation: -100
            }
          ]
        }
      ],
      scrollwheel: false,
      center: {
        lat: centerLat,
        lng: centerLng
      },
      zoom: mapZoom
    });
    _nc.maps[markerName] = new google.maps.Marker({
      position: markerPosition,
      icon: markerImg,
      map: _nc.maps[mapName]
    });
  });
};
