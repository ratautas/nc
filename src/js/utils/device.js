if (typeof _nc === 'undefined') {
  var _nc = {};
}

_nc.device = {
  isSet: false,
  isPortrait: screen.width < screen.height,
  ratio: screen.width / screen.height,
  isTouch: typeof window.ontouchstart !== 'undefined',
  isMob: screen.width < 768,
  isTablet: screen.width < 1025 && screen.width > 767,
  isLaptop: screen.width > 1024,
  winW: window.innerWidth,
  winH: window.innerHeight,
  bodyW: document.body.clientWidth,
  bodyH: document.body.clientHeight,
  scrollbarW: window.innerWidth - document.body.clientWidth,
  hasScrollbar: document.body.clientWidth !== window.innerWidth,
  isIOS: ['iPad', 'iPhone', 'iPod'].indexOf(navigator.platform) >= 0,
  OS: null,
  ua: window.navigator.userAgent
};

if (_nc.device.ua.indexOf('Windows NT 10.0') != -1) _nc.device.OS = 'win-10';
if (_nc.device.ua.indexOf('Windows NT 6.2') != -1) _nc.device.OS = 'win-8';
if (_nc.device.ua.indexOf('Windows NT 6.1') != -1) _nc.device.OS = 'win-7';
if (_nc.device.ua.indexOf('Windows NT 5.1') != -1) _nc.device.OS = 'win-xp';
if (_nc.device.ua.indexOf('Mac') != -1) _nc.device.OS = 'mac-os';
if (_nc.device.ua.indexOf('Linux') != -1) _nc.device.OS = 'linux';

document.body.classList.add(_nc.device.isIOS ? 'is-ios' : 'no-ios');
document.body.classList.add(_nc.device.isTouch ? 'is-touch' : 'no-touch');
document.body.classList.add(_nc.device.isMob ? 'is-mob' : 'no-mob');
document.body.classList.add(
  _nc.device.isTablet && _nc.device.isTouch ? 'is-tablet' : 'no-tablet'
);
document.body.classList.add(_nc.device.isLaptop ? 'h' : 'no-laptop');
document.body.classList.add(_nc.device.OS);

_nc.device.check = function() {
  _nc.device.isSet = false;
  _nc.device.winW = window.innerWidth;
  _nc.device.winH = window.innerHeight;
  _nc.device.bodyW = document.body.clientWidth;
  _nc.device.bodyH = document.body.clientHeight;
  _nc.device.ratio = window.innerWidth / window.innerHeight;
  _nc.device.scrollbarW = window.innerWidth - document.body.clientWidth;
  _nc.device.hasScrollbar = document.body.clientWidth !== window.innerWidth;
};

_nc.device.set = function() {
  if (!_nc.device.isSet) {
    _nc.device.isSet = true;
    setTimeout(function() {
      _nc.device.check();
    }, 100);
  }
};
