$('[data-toggle]').each(function() {
  var $triggers = $(this).find('[data-toggle-trigger]')
  var $contents = $(this).find('[data-toggle-content]')
  var activeIndex = $(this)
    .find('[data-toggle-section].is-active')
    .index()
  $triggers.click(function() {
    var $section = $(this).closest('[data-toggle-section]')
    var $content = $section.find('[data-toggle-content]')
    if ($section.hasClass('is-active')) {
      $content.slideUp(220, function() {
        $section.removeClass('is-active')
      })
    } else {
      $content.slideDown(220, function() {
        $section.addClass('is-active')
      })
    }
  })
})
