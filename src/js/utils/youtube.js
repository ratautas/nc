if (typeof _nc === 'undefined') {
  var _nc = {};
}

_nc.videos = {};

_nc.videos.init = function() {
  if ($('[data-video]').length) {
    var tag = document.createElement('script');
    var firstScriptTag = document.getElementsByTagName('script')[0];
    _nc.videos.players = [];
    tag.src = 'https://www.youtube.com/iframe_api';
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    $('[data-video]').each(function(i) {
      var player = {};
      var $this = $(this);
      player.$obj = $this.find('[data-video-player]');
      player.$trigger = $this.find('[data-video-trigger]');
      player.ready = false;
      player.videoId = player.$obj.attr('data-video-player');
      player.start = function(e) {
        if (e.data == YT.PlayerState.PLAYING && _nc.videos.players[i].ready) {
          _nc.videos.players[i].ready = true;
        }
        _nc.fade.out(_nc.videos.players[i].$trigger);
      };
      player.setReady = function(e) {};
      _nc.videos.players.push(player);
    });
  }
};

function onYouTubeIframeAPIReady() {
  _nc.videos.players.forEach(function(o, i) {
    o.player = new YT.Player(_nc.videos.players[i].$obj[0], {
      height: o.h,
      width: o.w,
      videoId: o.videoId,
      events: {
        onReady: o.setReady,
        onStateChange: o.start
      }
    });
  });
}
