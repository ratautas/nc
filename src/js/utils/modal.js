if (typeof _nc === 'undefined') {
  var _nc = {};
}

_nc.modal = {};

_nc.modal.open = function(id) {
  var $modal = $('[data-modal="' + id + '"]');
  _nc.modal.isOpen = true;
  _nc.modal.active = id;
  _nc.fade.in($modal);
  _nc.scrollFix();
};

_nc.modal.close = function() {
  var $activeModal = $('[data-modal].is-active');
  if ($activeModal.length) {
    _nc.modal.isOpen = false;
    _nc.fade.out($activeModal);
    _nc.scrollUnFix();
  }
};

$(document).on('click', '[data-modal-trigger]', function() {
  _nc.modal.open($(this).attr('data-modal-trigger'));
});

$(document).on('click', '[data-modal-close]', function() {
  _nc.modal.close();
});

$(document).on('keydown', 'body', function(e) {
  var key = Number(e.keyCode);
  if (key === 27) {
    _nc.modal.close();
  }
});

$(document).on('click', '[data-modal]', function(e) {
  console.log($(e.target));
  if (!$(e.target).closest('[data-modal-prevent]').length) {
    _nc.modal.close();
  }
});
