_nc.monitor = scrollMonitor.createContainer($('[data-page]')[0]);
_nc.watchers = {};
_nc.watchers = {};
_nc.watchAnimating = false;

$('[data-watch]').each(function(i, $o) {
  _nc.watchers['watcher' + i] = _nc.monitor.create($o);
  _nc.watchers['watcher' + i].enterViewport(function() {
    _nc.watchers['watcher' + i].watchItem.classList.add('is-active');
  });
});

// _nc.watchAnimate = function() {
//   if ($('[data-watch].is-ready').length && !_nc.watchAnimating) {
//     _nc.watchAnimating = true;
//     var $el = $('[data-watch].is-ready').eq(0);
//     $el.addClass('is-active');
//     $el.removeClass('is-ready');
//     setTimeout(function() {
//       _nc.watchAnimate();
//       _nc.watchAnimating = false;
//     }, 300);
//   }
// };

// $('[data-watch]').each(function(i, $o) {
//   _nc.watchers['watcher' + i] = _nc.monitor.create($o);
//   _nc.watchers['watcher' + i].isDirty = false;
//   _nc.watchers['watcher' + i].enterViewport(function() {
//     if (!_nc.watchers['watcher' + i].isDirty) {
//       _nc.watchers['watcher' + i].watchItem.classList.add('is-ready');
//       _nc.watchAnimate();
//       _nc.watchers['watcher' + i].isDirty = true;
//       // _nc.watchAnimating = false;
//     }
//   });
// });
