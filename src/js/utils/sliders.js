if (typeof _nc === 'undefined') {
  var _nc = {};
}

_nc.slider = {};

_nc.slider.init = function() {
  var $slider = $('[data-swiper]');
  var $swiper = $slider.find('[data-swiper-swiper]');
  _nc.slider.swiper = new Swiper($swiper, {
    loop: true,
    speed: 0,
    keyboard: true,
    slideActiveClass: 'is-active',
    preventClicks: false,
    simulateTouch: false,
    preventClicksPropagation: false
  });
};

$(document).on('click', '[data-swiper-prev]', function(e) {
  _nc.slider.swiper.slidePrev(0);
});

$(document).on('click', '[data-swiper-next]', function(e) {
  _nc.slider.swiper.slideNext(0);
});