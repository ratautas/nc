_nc.appReady = function() {
  _nc.demo.init()
  window.onresize = _db(function() {
    console.log('debounce')
  }, 300)
}

// ///// CLICKAWAY
// $(document).on('click touchend', '[data-page]', function(e) {
//   if (!$(e.target).closest('[data-filter]').length) {
//     e.stopPropagation();
//     $('[data-filter]').removeClass('is-active');
//   }
// });

window.onload = _nc.appReady()

$(document).on('click', '[data-scrollto]', function() {
  var $target = $($('[data-scrollto]').attr('data-scrollto'))
  if ($target.length) {
    TweenLite.to('[data-page]', 1, { scrollTo: { y: $target } })
  }
})
