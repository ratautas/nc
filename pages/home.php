<?php include '../partials/head.php'; ?>
<div class="page__top top" data-top>
  <?php include '../partials/top.php'; ?>
</div>
<div class="page__textpage textpage">
  <form class="form">
    <div class="form__row">
      <label class="form__item">
        <input type="text" class="form__input" placeholder=" ">
        <span class="form__placeholder">placeholder</span>
      </label>
      <label class="form__item form__item--checkbox">
        <input type="checkbox" class="form__native">
        <i class="form__control form__control--checkbox"></i>
        <span class="form__inlabel">label</span>
      </label>
    </div>
    <div class="form__row">
      <div class="form__item">
        <button type="submit" class="form__submit btn">SUBMIT!</button>
      </div>
    </div>
    <div class="form__row">
      <div class="form__item">
        <button type="submit" class="form__submit btn btn--inv">SUBMIT!</button>
      </div>
    </div>
  </form>
</div>
<?php include '../partials/footer.php'; ?>