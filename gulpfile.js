var devURL = 'localhost/nc'

var gulp = require('gulp')
var runSequence = require('run-sequence').use(gulp)
var rename = require('gulp-rename')
var changed = require('gulp-changed')
var newer = require('gulp-newer')
var concat = require('gulp-concat')
var uglify = require('gulp-uglify')
var gutil = require('gulp-util')

var imagemin = require('gulp-imagemin')

var favicons = require('gulp-favicons')

var browserSync = require('browser-sync').create()

var less = require('gulp-less')
var lessChanged = require('gulp-less-changed')
var sourcemaps = require('gulp-sourcemaps')

var LessAutoPrefixPlugin = require('less-plugin-autoprefix')
var LessAutoPrefix = new LessAutoPrefixPlugin({
  browsers: ['last 8 versions']
})
var LessGroupMediaQueries = require('less-plugin-group-css-media-queries')
var LessPluginCleanCSS = require('less-plugin-clean-css')
var LessCleanCSS = new LessPluginCleanCSS({
  advanced: false
})

gulp.task('lessDev', function() {
  return gulp
    .src('src/index.less')
    .pipe(lessChanged())
    .pipe(sourcemaps.init())
    .pipe(less())
    .pipe(rename('app.css'))
    .pipe(sourcemaps.write('/'))
    .pipe(gulp.dest('assets/css'))
    .pipe(browserSync.stream())
})

gulp.task('lessBuild', function() {
  return gulp
    .src('src/index.less')
    .pipe(lessChanged())
    .pipe(sourcemaps.init())
    .pipe(
      less({
        plugins: [LessAutoPrefix, LessGroupMediaQueries, LessCleanCSS]
      })
    )
    .pipe(rename('app.css'))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('assets/css/'))
    .pipe(browserSync.stream())
})

gulp.task('jsConcatDev', function() {
  return gulp
    .src([
      'src/js/core/*.js',
      'src/js/libs/**/*.js',
      'src/js/libs/*.js',
      'src/js/utils/*.js',
      'src/js/*.js'
    ])
    .pipe(concat('scripts.js'))
    .pipe(gulp.dest('assets/js/'))
})

gulp.task('jsDev', function() {
  return gulp
    .src('src/index.js')
    .pipe(rename('app.js'))
    .pipe(gulp.dest('assets/js/'))
})

gulp.task('jsBuild', function() {
  return gulp
    .src(['assets/js/scripts.js', 'assets/js/app.js'])
    .pipe(
      uglify().on('error', function(err) {
        gutil.log(gutil.colors.red('[Error]'), err.toString())
        this.emit('end')
      })
    )
    .pipe(concat('app.min.js'))
    .pipe(gulp.dest('assets/js'))
})

gulp.task('copyFonts', function() {
  return gulp.src('src/fonts/**/*').pipe(gulp.dest('assets/fonts'))
})

gulp.task('images', function() {
  gulp
    .src('src/img/**/*')
    .pipe(newer('assets/img'))
    .pipe(
      imagemin([
        imagemin.gifsicle({
          interlaced: true
        }),
        imagemin.jpegtran({
          progressive: true
        }),
        imagemin.optipng({
          optimizationLevel: 5
        })
      ])
    )
    .pipe(gulp.dest('assets/img'))
  gulp
    .src('media/**/*')
    .pipe(
      imagemin([
        imagemin.gifsicle({
          interlaced: true
        }),
        imagemin.jpegtran({
          progressive: true
        }),
        imagemin.optipng({
          optimizationLevel: 5
        })
      ])
    )
    .pipe(gulp.dest('media'))
})

gulp.task('favicon', function() {
  return gulp
    .src('./src/img/favicon.png')
    .pipe(
      favicons({
        background: 'transparent',
        replace: true,
        icons: {
          android: true, // Create Android homescreen icon. `boolean`
          appleIcon: true, // Create Apple touch icons. `boolean` or `{ offset: offsetInPercentage }`
          appleStartup: true, // Create Apple startup images. `boolean`
          favicons: true // Create regular favicons. `boolean`
        }
      })
    )
    .pipe(gulp.dest('./assets/img/favicons'))
})

gulp.task('dev', ['jsConcatDev', 'lessDev', 'jsDev'], function() {
  browserSync.init({
    proxy: {
      target: devURL
    }
  })
  gulp.watch('src/**/*.less', ['lessDev'])
  gulp.watch('src/**/*.js', ['jsConcatDev']).on('change', browserSync.reload)
  gulp.watch('src/*.js', ['jsDev']).on('change', browserSync.reload)
  gulp.watch('**/*.php').on('change', browserSync.reload)
  gulp.watch('**/*.html').on('change', browserSync.reload)
  gulp.watch('*.html').on('change', browserSync.reload)
  gulp.watch('*.php').on('change', browserSync.reload)
})

gulp.task('build', ['lessBuild', 'jsBuild', 'images', 'copyFonts'])

gulp.task('default', function(callback) {
  return runSequence('dev', callback)
})
